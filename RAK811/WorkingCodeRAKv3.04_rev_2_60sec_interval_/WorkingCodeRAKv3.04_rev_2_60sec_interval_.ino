/*
The Water Level Monitoring code contains the following libraries

RAK811 - modified libraries
New Ping Library for the Ultrasonic Sensor
DHT11 - Temperature and Humidity Library
Software Serial Library


*/

#include <RAK811.h>

//Arduino WorkingCode
#include "NewPing.h";
#include <DHT.h>
#include "SoftwareSerial.h"

#define trigPin 7
#define echoPin 8
#define maxDistance 400
#define DHTPIN 13
#define DHTTYPE DHT11

#define WORK_MODE LoRaWAN   //  LoRaWAN or LoRaP2P
#define JOIN_MODE OTAA    //  OTAA or ABP

int i;

DHT dht(DHTPIN, DHTTYPE);

NewPing sonar(trigPin, echoPin, maxDistance);


//#if JOIN_MODE == OTAA
//ZARAK1
/*
String DevEui = "3739343570376105";               //at+set_config=dev_eui:3739343570376105&app_eui:70B3D57ED001AB1E&app_key:7E8A4947B0B1EFC14885C00057A9D7AA
String AppEui = "70B3D57ED001AB1E";
String AppKey = "7E8A4947B0B1EFC14885C00057A9D7AA";
*/

/*#else JOIN_MODE == ABP
String NwkSKey = "3432567afde4525e7890cfea234a5821";
String AppSKey = "a48adfc393a0de458319236537a11d90";
String DevAddr = "00112233";
*/
//ZARAK2
/*
String DevEui = "3739343555377301"; 
String AppEui = "70B3D57ED001AB1E";
String AppKey = "ECE7506BBD934A794AF88D8CE9F70C82";
*/
//ZARAK3
/*
String DevEui = "00799D79202174A7"; 
String AppEui = "70B3D57ED001AB1E"; 
String AppKey = "03175965FA0E36EAF0FF7765B20B637A";
*/
//ZARAK4
/*
String DevEui = "00C5EE9F29A0F027"; 
String AppEui = "70B3D57ED001AB1E"; 
String AppKey = "01522A032CF7F2074E9DE129A514D032";
*/
//ZARAK5
String DevEui = "0011002A2336D2B5"; 
String AppEui = "70B3D57ED001AB1E"; 
String AppKey = "EBB16A44F2743F8AC9C302A81BCBCC9F";

#define TXpin 11   // Set the virtual serial port pins
#define RXpin 10
#define ATSerial Serial

SoftwareSerial DebugSerial(RXpin,TXpin);    // Declare a virtual serial port
//char* buffer = "72616B776972656C657373";

RAK811 RAKLoRa(ATSerial,DebugSerial);

//string to hold the response of a command in rak811
String response = "";

float duration, distance;
float hum;
float temp;
float soundsp;
float soundcm;

int iterations = 10;


void setup() {

ATSerial.begin(115200); // Note: Please manually set the baud rate of the WisNode device to 9600.

dht.begin();
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

RAKLoRa.rk_reset(0);
delay(1000);

RAKLoRa.rk_setWorkingMode(WORK_MODE);
delay(500);

RAKLoRa.rk_initOTAA(DevEui, AppEui, AppKey);
delay(500);

RAKLoRa.rk_joinLoRaNetwork(JOIN_MODE);
delay(500);

RAKLoRa.rk_join(JOIN_MODE);
response = "";
  while(Serial.available()){
    char ch = Serial.read();
    response += ch;
  }
  Serial.println(response);

delay(60000);

}

void(* resetFunc) (void) = 0; //declare reset function @ address 0


void loop() {

 for(i=0; i<120; i++)
 {
  Serial.print("Counter =");
  Serial.println(i);
  // Read sensor values and multiply by 100 to effictively have 2 decimals
  uint16_t humidity = dht.readHumidity(false) * 100;

  // false: Celsius (default)
  // true: Farenheit
  uint16_t temperature = dht.readTemperature(false) * 100;

hum = dht.readHumidity();
temp=dht.readTemperature();

soundsp = 331.4 + (0.606 * temp) + (0.0124 * hum);

soundcm = soundsp / 10000;

duration = sonar.ping_median(iterations);
distance = (duration /2) * soundcm;

uint16_t dist = ((duration /2) * soundcm) * 100;

Serial.print("Humidity");
Serial.print(hum);
Serial.print(",");
Serial.print("Temperature");
Serial.print(temp);
Serial.print(",");

 Serial.print("Water Level = ");
  if (distance >= 400 ||distance <=2) {
    Serial.println("Out of Range");
  }
  else {
    Serial.print(distance/100);
    Serial.println( " m");
    delay(500);
  }
  // Split both words (16 bits) into 2 bytes of 8
  byte payload[6];
  payload[0] = highByte(temperature);
  payload[1] = lowByte(temperature);
  payload[2] = highByte(humidity);
  payload[3] = lowByte(humidity);
  payload[4] = highByte(dist);
  payload[5] = lowByte(dist);


char str[32] = "";
array_to_string(payload, 6, str);
//Serial.println(str);
delay(1000);

RAKLoRa.rk_sendData(2,str);
//RAKLoRa.rk_sendData(1,"092624232321");
delay (60000);
}
 resetFunc();  //call reset
}

void array_to_string(byte array[], unsigned int len, char buffer[])
{
    for (unsigned int i = 0; i < len; i++)
    {
        byte nib1 = (array[i] >> 4) & 0x0F;
        byte nib2 = (array[i] >> 0) & 0x0F;
        buffer[i*2+0] = nib1  < 0xA ? '0' + nib1  : 'A' + nib1  - 0xA;
        buffer[i*2+1] = nib2  < 0xA ? '0' + nib2  : 'A' + nib2  - 0xA;
    }
    buffer[len*2] = '\0';

}

